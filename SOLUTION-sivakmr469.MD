# Terraform AWS ECS Java
##### Author: Sivakumar Reddy Mettukuru
##### Email: sivakmr469@gmail.com

## This document consists of two parts
1. Creating AWS infrastructure with terraform
2. Deploying Springboot Docker application with terraform

## Techonolgy stack
* GitLab CICD
* Terraform
* AWS
* Springboot
* Docker

#### APP REST API

##### health check  
**URL:** http://echo-project-alb-1467975631.ap-southeast-1.elb.amazonaws.com/api/v1/health  
**Method:** GET

##### IP-Address API
URL: http://echo-project-alb-1467975631.ap-southeast-1.elb.amazonaws.com/api/v1/echo  
Method: POST  
Content-Type: application/json  
Request Body:
{
    "name": "some name",
    "mobileNmber": "somenumber"
}  
Sample Response:

## Creating AWS infrastructure with terraform

##### view only credentials
**URL** : https://084767242532.signin.aws.amazon.com/console  
**Username** : read-admin  
**Password** : read-admin345

##### Source Code and setup
**URL** : https://gitlab.com/sivakmr469/terraform-ivs.git
Clone the code, to run this successfully you need to setup the below 2 variables in Setting --> CICD --> Variables  
1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY  
  
Click protected while creating the variables and make sure you protected your branch to use these variables.  
  
Then just go and run the pipeline in CICD --> Pipeline section  
Once the pipeline is success you can see output section as below.  
```
alb_security_group = [
  "sg-0e4e2cc43ede34470",
]
container_image = "084767242532.dkr.ecr.ap-southeast-1.amazonaws.com/echo-project"
ecs_arn = "arn:aws:ecs:ap-southeast-1:084767242532:cluster/echo-project-cluster"
ecs_security_group = [
  "sg-someID",
]
private_subnets = [
  "subnet-someID",
  "subnet-someID",
]
public_subnets = [
  "subnet-someID",
  "subnet-someID",
]
vpc_id = "vpc-someID"
```
copy this for next section to deploy the application

## Deploy Java application with terraform

##### Source Code and setup
**URL** : https://gitlab.com/sivakmr469/echo-project.git
Clone the code, to run this successfully you need to setup the below 2 variables in Setting --> CICD --> Variables  
1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY  
  
Click protected while creating the variables and make sure you protected your branch to use these variables.  

Before trigger the pipeline you need to paste the above copied block in the below file.  
```
echo-project/terraform/terraform.tfvars
```
Then just go and run the pipeline in CICD --> Pipeline section  
Once the pipeline is success you can see output section as below.  
```
Outputs:
app_base_url = "echo-project-alb-1467975631.ap-southeast-1.elb.amazonaws.com"
```
this is the base URL. You can use this URL to perform post action as shown in APP REST API section above [here](#app-rest-api)